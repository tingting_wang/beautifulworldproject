﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace BeautifulWorld.ViewModel
{
	public class MainWindowViewModel
	{
		private RoutedCommand _testLinQCommand;

		public RoutedCommand TestLinQCommand
		{
			get { return _testLinQCommand; }
			set { _testLinQCommand = value; }
		}

		public string TestLinQ { get; }

		public MainWindowViewModel()
		{
			TestLinQCommand = new RoutedCommand(TestLinQ,typeof(MouseButton));
		}

	}
}

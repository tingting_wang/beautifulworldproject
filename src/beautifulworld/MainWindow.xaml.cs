using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NServiceKit.Text;

namespace BeautifulWorld
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		public void btnLinQ_Click(object sender, RoutedEventArgs e)
		{
			int[] numbers = { 5, 10, 8, 3, 6, 12 };

			List<Student> students = new List<Student>{

				new Student{ StudentId ="1111",StudentName="Wang",Scores =numbers.ToList()},
				new Student{ StudentId="2222",StudentName="Zhang",Scores =numbers.Select(i=>i+100).ToList()}

			};

			//Method syntax:
			IEnumerable<int> numQuery2 = numbers.Where(num =>
			{
				num++;
				if (num % 2 == 0)
					return true;
				return false;
			})
			.OrderBy(n => n).ToArray();

			var numQuery3 = numbers.Select(num => num + 10);

			var selectedStudent = students.Where(s => s.Scores.Sum() > 100);

			foreach (Student student in selectedStudent)
			{
				Console.Write($"id ={student.StudentId}  name ={student.StudentName}");
			}

			foreach (int i in numQuery3)
			{
				Console.Write(i + " ");
			}
			Console.WriteLine(System.Environment.NewLine);
			foreach (int i in numQuery2)
			{
				Console.Write(i + " ");
			}
			MessageBox.Show("End Button Click.");
		}
	}

	public class Student
	{
		public string StudentId;
		public string StudentName { get; set; }

		private List<int> _scores;
		public List<int> Scores
		{
			set => _scores = value;
			get => _scores ?? new List<int>();
		}
	}
}

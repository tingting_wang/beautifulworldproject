﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeautifulWorldUnitTestProject
{
	public class ApplicationSession
	{
		protected const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
		private const string WpfAppId = @"C:\Users\tingting.wang\beautifulworldproject\src\beautifulworld\bin\Debug\netcoreapp3.1\BeautifulWorld.exe";

		protected static WindowsDriver<WindowsElement> session;

		public static void Setup(TestContext testContext)
		{
			if (session == null)
			{
				DesiredCapabilities appCapabilities = new DesiredCapabilities();
				appCapabilities.SetCapability("app", WpfAppId);
				appCapabilities.SetCapability("deviceName", "WindowsPC");
				session = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appCapabilities);
				Assert.IsNotNull(session);

				// Set implicit timeout to 1.5 seconds to make element search to retry every 500 ms for at most three times
				session.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1.5);
			}
		}
		public static void TearDown()
		{
			// Close the application and delete the session
			if (session != null)
			{
				session.Quit();
				session = null;
			}
		}
	}
}

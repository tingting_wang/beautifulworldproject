using Microsoft.VisualStudio.TestTools.UnitTesting;
using BeautifulWorld;
using System;
using System.Windows.Threading;

namespace BeautifulWorldUnitTestProject
{
	[TestClass]
	public class MainWindowUnitTest : ApplicationSession
	{

		[ClassInitialize]
		public static void ClassInitialize(TestContext testContext)
		{
			Setup(testContext);
		}
		[ClassCleanup]
		public static void ClassCleanup()
		{
			TearDown();
		}

		[TestInitialize]
		public void Clear()
		{ }

		[TestMethod]
		public void TestButtonClick()
		{
			var btn = session.FindElementByAccessibilityId("btnLinQ");
			Assert.IsNotNull(btn);

			try
			{
				btn.Click();
			}
			catch (Exception)
			{
				Assert.Fail("Something wrong!");
			}
		}
	}
}
